FROM ubuntu:14.04
MAINTAINER Martin Bachmann<m.bachmann@insign.ch>

RUN useradd sbt
RUN mkdir /home/sbt
RUN chown -R sbt /home/sbt

# Set locales
RUN locale-gen en_GB.UTF-8
ENV LANG en_GB.UTF-8
ENV LC_CTYPE en_GB.UTF-8

# Install dependencies
RUN apt-get update && apt-get install -y \
        git \
        build-essential \
        curl \
        wget \
        zip \
        unzip \
        software-properties-common \
    && rm -rf /var/lib/apt/lists/*

# grab gosu for easy step-down from root
RUN gpg --keyserver pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4
RUN apt-get update && apt-get install -y --no-install-recommends ca-certificates wget && rm -rf /var/lib/apt/lists/* \
	&& wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/1.2/gosu-$(dpkg --print-architecture)" \
	&& wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/1.2/gosu-$(dpkg --print-architecture).asc" \
	&& gpg --verify /usr/local/bin/gosu.asc \
	&& rm /usr/local/bin/gosu.asc \
	&& chmod +x /usr/local/bin/gosu \
	&& apt-get purge -y --auto-remove ca-certificates wget

# Install Oracle Java 8
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections \
    && echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee /etc/apt/sources.list.d/webupd8team-java.list \
    && echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee /etc/apt/sources.list.d/webupd8team-java.list \
    && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886 \
    && apt-get update && apt-get install -y \
        oracle-java8-installer \
        oracle-java8-set-default \
    && rm -rf /var/lib/apt/lists/*

# Install SBT
RUN wget https://dl.bintray.com/sbt/debian/sbt-0.13.7.deb \
    && dpkg -i sbt-0.13.7.deb \
    && rm sbt-0.13.7.deb

# Fix SBT launcher
RUN sed -i 's/agentlib:jdwp:transport/agentlib:jdwp=transport/g' /usr/share/sbt-launcher-packaging/bin/sbt-launch-lib.bash


RUN mkdir -p /usr/src/app/source /usr/src/app/build
WORKDIR /usr/src/app/source
VOLUME /usr/src/app/source

EXPOSE 9000
EXPOSE 9999

COPY ./entrypoint.sh /

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["sbt", "-jvm-debug", "9999", "run"]