#!/usr/bin/env bash

docker-compose -f jenkins.yml -p play-cms-demo up -d

while ! echo exit | nc -z -w 3 localhost 8081; do sleep 3; done;
while ! curl -s http://localhost:8081 | grep "Manage Jenkins"; do echo "Waiting for Jenkins to start.." && sleep 3; done;
echo "\
\
Jenkins started";

# check if first run then install plugins and import job
if ! java -jar ./jenkins/jenkins-cli.jar -s http://localhost:8081/ list-plugins | grep "GIT plugin"
then
    java -jar ./jenkins/jenkins-cli.jar -s http://localhost:8081/ install-plugin ./jenkins/scm-api.hpi -deploy
    java -jar ./jenkins/jenkins-cli.jar -s http://localhost:8081/ install-plugin ./jenkins/git-client.hpi -deploy
    java -jar ./jenkins/jenkins-cli.jar -s http://localhost:8081/ install-plugin ./jenkins/git.hpi -deploy -restart

    # waiting for restart
    while ! echo exit | nc -z -w 3 localhost 8081; do sleep 3; done;
    while ! curl -s http://localhost:8081 | grep "Manage Jenkins"; do echo "Waiting for Jenkins to start.." && sleep 3; done;

    java -jar ./jenkins/jenkins-cli.jar -s http://localhost:8081/ create-job Test_Play_cms_demo < ./jenkins/testjob.xml
fi