import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.authz.DefaultDomainPermission;
import ch.insign.playauth.authz.DomainPermission;
import ch.insign.playauth.authz.ObjectIdentity;
import ch.insign.playauth.controllers.actions.RequiresAuthenticationAction;
import ch.insign.playauth.controllers.actions.RequiresPermissionAction;
import ch.insign.playauth.party.ISOGender;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyType;
import ch.insign.playauth.party.address.EmailAddress;
import ch.insign.playauth.utils.CacheUtils;
import com.google.common.base.Throwables;
import org.apache.shiro.authc.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import play.api.mvc.RequestHeader;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.test.WithApplication;
import scala.concurrent.Await;
import scala.concurrent.duration.Duration;

import java.util.HashMap;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static play.mvc.Http.Status.FORBIDDEN;
import static play.mvc.Http.Status.OK;
import static play.mvc.Http.Status.SEE_OTHER;
import static play.test.Helpers.*;

/**
 * TODO: - add impersonation tests; - add more authorization tests; - add logout tests;
 */
public class AuthTest extends WithApplication {

	private static final DomainPermission<?> TEST_INSTANCE_PERMISSION = new DefaultDomainPermission<>(
			DummyBean.class.getName(), "name", new ObjectIdentity(DummyBean.class.getName(), UUID.randomUUID().toString()));

	private static final DomainPermission<?> TEST_CLASS_PERMISSION = new DefaultDomainPermission<>(
			DummyBean.class.getName(), "name", new ObjectIdentity(DummyBean.class.getName(), ObjectIdentity.ALL.getIdentifier()));

	private static final String PARTY_PWD = "sword";
	private static final String INCORRECT_PARTY_PWD = "wrong";


	@Before
	public void setUp() {
		start(fakeApplication(inMemoryDatabase(), fakeGlobal()));
		Http.Context.current.set(fakeContext());
	}

	@After
	public void tearDown() {
		CacheUtils.manager().clearAll();
		Http.Context.current.set(null);
	}

	@Test
	public void testLoginSuccess() {
		PlayAuth.executeWithTransaction(Http.Context.current(), () -> {
			Party party = createUnlockedParty();
			try {
				PlayAuth.login(new UsernamePasswordToken(party.getEmail(), PARTY_PWD));
			} catch (AuthenticationException e) {
				fail("Unlocked party can login with valid UsernamePasswordToken");
			}
		});
	}

	@Test(expected = LockedAccountException.class)
	public void testLockedAccountLoginFailure() {
		PlayAuth.executeWithTransaction(Http.Context.current(), () -> {
			Party party = createLockedParty();
			PlayAuth.login(new UsernamePasswordToken(party.getEmail(), PARTY_PWD));
		});
	}

	@Test(expected = IncorrectCredentialsException.class)
	public void testIncorrectCredentialsLoginFailure() {
		PlayAuth.executeWithTransaction(Http.Context.current(), () -> {
			Party party = createUnlockedParty();
			PlayAuth.login(new UsernamePasswordToken(party.getEmail(), INCORRECT_PARTY_PWD));
		});
	}

	@Test(expected = UnknownAccountException.class)
	public void testUnknownAccountLoginFailure() {
		PlayAuth.executeWithTransaction(Http.Context.current(), () -> {
			PlayAuth.login(new UsernamePasswordToken("unknown@insign.ch", INCORRECT_PARTY_PWD));
		});
	}

	@Test
	public void testRequiresAuthenticationFailure() {
		Result result = callAction(okActionWith(new RequiresAuthenticationAction()));

		assertEquals(SEE_OTHER, status(result));
		assertEquals("/", redirectLocation(result));
	}

	@Test
	public void testRequiresAuthenticationSuccess() {
		PlayAuth.executeWithTransaction(Http.Context.current(), () -> {
			Party party = createUnlockedParty();
			PlayAuth.login(new UsernamePasswordToken(party.getEmail(), PARTY_PWD));
		});

		Result result = callAction(okActionWith(new RequiresAuthenticationAction()));

		assertEquals(OK, status(result));
		assertEquals("success", contentAsString(result));
	}

	@Test
	public void testRequiresInstancePermissionSuccess() {
		PlayAuth.executeWithTransaction(Http.Context.current(), () -> {
			Party party = createUnlockedParty();
			PlayAuth.getAccessControlManager().allowPermission(party, TEST_CLASS_PERMISSION);
			PlayAuth.login(new UsernamePasswordToken(party.getEmail(), PARTY_PWD));
		});

		Result result = callAction(okActionWith(new RequiresPermissionAction<Object>() {
			@Override
			public DomainPermission<?> permission(Http.Context ctx) {
				return TEST_INSTANCE_PERMISSION;
			}
		}));

		assertEquals(OK, status(result));
		assertEquals("success", contentAsString(result));
	}

	// Todo: fix possible permission bug
	@Ignore
	@Test
	public void testRequiresInstancePermissionFailure1() {
		PlayAuth.executeWithTransaction(Http.Context.current(), () -> {
			Party party = createUnlockedParty();
			PlayAuth.login(new UsernamePasswordToken(party.getEmail(), PARTY_PWD));
		});

		Result result = callAction(okActionWith(new RequiresPermissionAction<Object>() {
			@Override
			public DomainPermission<?> permission(Http.Context ctx) {
				return TEST_INSTANCE_PERMISSION;
			}
		}));

		assertEquals(FORBIDDEN, status(result));
	}

	@Test
	public void testRequiresInstancePermissionFailure2() {
		PlayAuth.executeWithTransaction(Http.Context.current(), () -> {
			Party party = createUnlockedParty();
			PlayAuth.getAccessControlManager().allowPermission(party, TEST_CLASS_PERMISSION);
			PlayAuth.getAccessControlManager().denyPermission(party, TEST_INSTANCE_PERMISSION);
			PlayAuth.login(new UsernamePasswordToken(party.getEmail(), PARTY_PWD));
		});

		Result result = callAction(okActionWith(new RequiresPermissionAction<Object>() {
			@Override
			public DomainPermission<?> permission(Http.Context ctx) {
				return TEST_INSTANCE_PERMISSION;
			}
		}));

		assertEquals(FORBIDDEN, status(result));
	}


	/* TEST HELPERS */

	/** Composes sample "ok" action with the given action */
	private Action<?> okActionWith(Action<?> action) {
		action.delegate = new Action() {
			@Override
			public F.Promise<Result> call(Http.Context ctx) throws Throwable {
				return F.Promise.pure(ok("success"));
			}
		};

		return action;
	}

	private Result callAction(Action<?> action) {
		try {
			return Await.result(action.call(Http.Context.current()).wrapped(), Duration.Inf());
		} catch (Throwable t) {
			throw Throwables.propagate(t);
		}
	}

	private Party createLockedParty() {
		Party party = PlayAuth.getPartyManager().create(
				"Locked Party",
				PARTY_PWD,
				new EmailAddress("locked@insign.ch"),
				ISOGender.MALE,
				PartyType.PERSON);

		party.setLocked(true);

		return party;
	}

	private Party createUnlockedParty() {
		Party party = PlayAuth.getPartyManager().create(
				"Unlocked Party",
				PARTY_PWD,
				new EmailAddress("unlocked@insign.ch"),
				ISOGender.MALE,
				PartyType.PERSON);

		party.setLocked(false);

		return party;
	}

	private Http.Context fakeContext() {
		Http.Request request = mock(Http.Request.class);
		when(request.path()).thenReturn("/");

		return new Http.Context(
				13L,
				mock(RequestHeader.class),
				request,
				new HashMap<>(),
				new HashMap<>(),
				new HashMap<>());
	}
}
