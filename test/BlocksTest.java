import ch.insign.cms.models.*;
import ch.insign.cms.permissions.BlockPermission;
import ch.insign.commons.db.JPA;
import ch.insign.commons.db.MString;
import ch.insign.commons.i18n.Language;
import ch.insign.commons.util.Configuration;
import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.authz.DomainPermission;
import ch.insign.playauth.authz.SecurityIdentity;
import ch.insign.playauth.party.ISOGender;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyType;
import ch.insign.playauth.party.address.EmailAddress;
import ch.insign.playauth.utils.CacheUtils;
import ch.insign.playshiro.session.PlayShiroSession;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.joda.time.DateTime;
import org.junit.*;
import play.mvc.Http;
import play.mvc.Result;
import play.test.FakeRequest;

import javax.persistence.RollbackException;
import java.util.*;

import static ch.insign.playauth.PlayAuth.getAccessControlManager;
import static org.junit.Assert.*;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.route;
import static play.test.Helpers.status;

/**
 * Tests blocks-creation and manipulation (CRUD)
 * Tests also:
 * - Adding CollectionBlocks to Pages
 * - Adding ContentBlocks to Collections
 * - Change Content of ContentBlock
 * - Delete Content of ContentBlock
 *
 * User: u.honegger@insign.ch
 * Date: 11/8/13
 * Time: 11:20 AM
 */
public class BlocksTest extends AbstractTest {

    /**
     * Add initial set of Blocks to work with in Tests
     */
    @Before
    public void populateBlockData() {
        //setupShiroThreadState();
        bindEntityManager();
        t_begin();

        em.createQuery("DELETE FROM NavigationItem").executeUpdate();
        em.createQuery("DELETE FROM AbstractBlock").executeUpdate();
        //AbstractTest.em.createQuery("DELETE FROM CollectionBlock ").executeUpdate();
        //AbstractTest.em.createQuery("DELETE FROM PageBlock ").executeUpdate();

        // This is the minimum setup required for tests
	    CMS.getSetup().addCoreCMSData();
	    CMS.getSetup().addErrorPages();
	    CMS.getSetup().onAppStart();

        // Add collectionBlocks

        CollectionBlock c1 = new CollectionBlock();
        c1.setKey("First Collection");
        c1.setName("First Collection");
        c1.save();

        CollectionBlock c2 = new CollectionBlock();
        c2.setKey("Second Collection");
        c2.setName("Second Collection");
        c2.save();

        // Add pageBlocks

        PageBlock homepage = new PageBlock();
        homepage.setName("Home Page");
        homepage.setKey(Setup.KEY_HOMEPAGE);
        try {
            homepage.createNavItem("en","/homepage", true);
            homepage.createNavItem("de","/homepage_de", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        homepage.save();

        PageBlock p1 = new PageBlock();
        p1.setName("First Page");
        p1.setKey("First Page");
	    try {
		    p1.createNavItem("en","/firstpage", true);
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
        p1.save();

        PageBlock p2 = new PageBlock();
        p2.setName("Second Page");
        p2.setKey("Second Page");
	    try {
		    p2.createNavItem("en","/secondpage", true);
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
        p2.save();

        // page3 is a sub-page of page2
        PageBlock p3 = new PageBlock();
        p3.setName("Third Page");
        p3.setKey("Third Page");
	    try {
		    p3.createNavItem("en","/thirdpage", true);
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
        p3.save();
        p3.setParentBlock(p2);
        p2.getSubBlocks().add(p3);


	    // page4 is used for trash testing
	    PageBlock p4 = new PageBlock();
	    p4.setName("Trashed Page");
	    p4.setKey("Trashed Page");
	    try {
		    p4.createNavItem("en","/trashedpage", true);
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
	    p4.save();
	    p4.setParentBlock(p2);
	    p2.getSubPages().add(p4);

	    // page5 is a sub-page of (to be trashed) page4
	    PageBlock p5 = new PageBlock();
	    p5.setName("Trashed Subpage");
	    p5.setKey("Trashed Subpage");
	    try {
		    p5.createNavItem("en","/trashedsubpage", true);
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
	    p5.save();
	    p5.setParentBlock(p4);
	    p4.getSubBlocks().add(p5);

	    // Trash it
	    p4.moveToTrash();

	    // Invisible page
	    PageBlock p6 = new PageBlock();
	    p6.setName("Invisible Page");
	    p6.setKey("Invisible Page");
	    try {
		    p6.createNavItem("en","/invisible", false);
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
	    p6.save();

	    // page7 is used for drafts testing
	    PageBlock p7 = new PageBlock();
	    p7.setName("Draft Page");
	    p7.setKey("Draft Page");
	    try {
		    p7.createNavItem("en","/draftpage", true);
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
	    p7.save();

	    // move page7 to drafts
	    PageBlock drafts = PageBlock.find.drafts();
	    p7.setParentBlock(drafts);
	    drafts.getSubPages().add(p7);

	    // page8 is a sub-page of draft page7
	    PageBlock p8 = new PageBlock();
	    p8.setName("Draft Subpage");
	    p8.setKey("Draft Subpage");
	    try {
		    p8.createNavItem("en","/draftsubpage", true);
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
	    p8.save();
	    p8.setParentBlock(p7);
	    p7.getSubBlocks().add(p8);

        // Add contentBlock

        ContentBlock contentBlock = new ContentBlock();
        contentBlock.setName("First Content");
        contentBlock.setKey("First Content");
        MString aString = new MString();
        aString.set("de", "I'm the very first content! formerly known as Hello World ;)");
        contentBlock.setContent(aString);
        contentBlock.save();

        // Add users with corresponding permissions

        Party superUser = PlayAuth.getPartyManager().create(
                "Super User",
                "secretword",
                new EmailAddress("super@insign.ch"),
                ISOGender.MALE,
                PartyType.PERSON);

        Party readerUser = PlayAuth.getPartyManager().create(
                "Reader User",
                "secretword",
                new EmailAddress("reader@insign.ch"),
                ISOGender.MALE,
                PartyType.PERSON);

        Party writerUser = PlayAuth.getPartyManager().create(
                "Writer User",
                "secretword",
                new EmailAddress("writer@insign.ch"),
                ISOGender.MALE,
                PartyType.PERSON);

	    Party unprivilegedUser = PlayAuth.getPartyManager().create(
			    "Unprivileged User",
			    "secretword",
			    new EmailAddress("unprivileged@insign.ch"),
			    ISOGender.MALE,
			    PartyType.PERSON);

        // Grant class-level permissions

        // superUser is superuser
	    getAccessControlManager().allowPermission(superUser, DomainPermission.ALL);
        // readerUser can read any page
	    getAccessControlManager().allowPermission(readerUser, BlockPermission.READ, PageBlock.class);
        // writerUser can write any page
	    getAccessControlManager().allowPermission(writerUser, BlockPermission.MODIFY, PageBlock.class);

        // Grant instance-level permissions

	    // restrict p2
	    getAccessControlManager().denyPermission(SecurityIdentity.ALL, BlockPermission.READ, p2);
        // readerUser can read p2
	    getAccessControlManager().allowPermission(readerUser, BlockPermission.READ, p2);
        // readerUser can write p2
	    getAccessControlManager().allowPermission(readerUser, BlockPermission.MODIFY, p2);
	    // readerUser can NOT write p3
	    getAccessControlManager().denyPermission(readerUser, BlockPermission.MODIFY, p3);

        // p3 inherits rights from p2

        t_commitAndClear();
    }

	@After
	public void cleanup() {
		CacheUtils.manager().clearAll();
		BlockCache.flush();
		Http.Context.current.set(null);
	}

    /**
     * Test if Collection Block can be added to Page
     */
    @Test
    public void retriveBlocks() {
        PageBlock page = PageBlock.find.byKey("First Page");
        CollectionBlock collection = CollectionBlock.find.byKey("First Collection");
        ContentBlock content = ContentBlock.find.byKey("First Content");

        assertNotNull(page);
        assertNotNull(collection);
        assertNotNull(content);
    }

    /**
     * Test if Blocks can be renamed
     */
    @Test
    public void renameNavItems() {
        ContentBlock content = ContentBlock.find.byKey("First Content");

        t_begin();

        content.setName("newName");
        content.setKey("newKey");
        content.save();

        t_commitAndClear();

        ContentBlock cb = ContentBlock.find.byKey("newKey");
        assertNotNull(cb);
        assertEquals("newName", cb.getName());
    }

    /**
     * Test if Blocks can be deleted
     */
    @Test
    public void deleteBlocks() {

	    JPA.withTransaction(() -> {
            PageBlock page = PageBlock.find.byKey("First Page");
            // FIXME: page.delete(); -- throws exception
        });

	    JPA.withTransaction(() -> {
            CollectionBlock collection = CollectionBlock.find.byKey("First Collection");
            collection.delete();
        });

	    JPA.withTransaction(() -> {
            ContentBlock content = ContentBlock.find.byKey("First Content");
            content.delete();
        });

        //assertNull(PageBlock.find.byKey("First Page"));
        assertNull(CollectionBlock.find.byKey("First Collection"));
        assertNull(ContentBlock.find.byKey("First Content"));
    }

    /**
     * Test if Cascading deletion of attached Blocks work
     */
    @Test
    public void cascadingBlockDeletion() {
//        t_begin();
        PageBlock page = PageBlock.find.byKey("First Page");
        CollectionBlock collection =  CollectionBlock.find.byKey("First Collection");
        ContentBlock content = ContentBlock.find.byKey("First Content");

        // TODO: figure out why Error happens by setting Subblocks via setSubBlock Method.
        // -> because tests are executed in parallel and JPA em does not like that

//        page.setSubBlocks(
//            Arrays.asList(
//                (AbstractBlock)collection
//            )
//        );
//        page.save();
//        t_commitAndClear();
//
//        t_begin();
//        collection.setSubBlocks(
//                Arrays.asList(
//                        (AbstractBlock)content
//                )
//        );
//        collection.save();
//
//        t_commitAndClear();


        // @TODO: Has to be fixed in a propper way! adjust onBeforeDelete of AbstractBlock... urs
//        collection.setParentBlock(PageBlock.find.byKey("First Page"));
//        collection.save();
//
//        content.setParentBlock(collection);
//        content.save();
//
//        t_commitAndClear();
//
//        t_begin();
//        page = PageBlock.find.byKey("First Page");
//        // Here we try to make a cascading delete
//        page.delete();
//
//        //collection = CollectionBlock.find.byKey("First Collection");
//        // Here we try to make a cascading delete
//        //collection.delete();
//
//
//        t_commitAndClear();
//
//        assertNull(PageBlock.find.byKey("First Page"));
//        assertNull(CollectionBlock.find.byKey("First Collection"));
//        assertNull(ContentBlock.find.byKey("First Content"));
    }

    /**
     * Test if Deletion of NavItem leads to cascading Delete of attached Blocks.
     * Should only happen if Block is not attached to other Nav Items
     */
    @Test
    public void cascadingNavItemDeletion() throws NavigationItem.VpathNotAvailableException, NavigationItem.VpathNotValidException {
        t_begin();
        NavigationItem nav1 = new NavigationItem();
        NavigationItem nav2 = new NavigationItem();

        PageBlock page = PageBlock.find.byKey("First Page");
        // Attach Page to both Menu-Entries
        nav1.setPage(page);
        nav1.setVirtualPath("/nav1");
        nav1.setPage(page);
        nav2.setVirtualPath("/nav2");
        nav2.setPage(page);

        nav1.save();
        nav2.save();
        t_commitAndClear();

        // Delete First NavigationItem
        t_begin();
        nav1 = NavigationItem.find.byVpath("/nav1");
        nav1.delete();
        t_commitAndClear();

        // Page should still exist, because linked to another NavItem
        assertNotNull(PageBlock.find.byKey("First Page"));


        // Delete Second NavigationItem
        t_begin();
        nav2 = NavigationItem.find.byVpath("/nav2");
        nav2.delete();
        t_commitAndClear();

        // Page should not exist anymore (cascade)
        // FIXME: assertion Tests if Page was deleted (because last Nav-Reference were deleted).
        // If Page should be deleted as well, cascading-functionality must be implemented!
        //assertNull(PageBlock.find.byKey("First Page"));
    }



    /**
     * Test if it's possible to add and Remove a CollectionBlock from and to Pages
     */
    @Test
    public void addCollectionToPage() throws ClassNotFoundException {

        PageBlock page = PageBlock.find.byKey("First Page");
        CollectionBlock firstCollection = CollectionBlock.find.byKey("First Collection");
        CollectionBlock secondCollection = CollectionBlock.find.byKey("Second Collection");

        t_begin();
        Template.addBlockToSlot(CollectionBlock.class, page, "leftCollectionBlock");
        page.save();
        t_commitAndClear();


        page = PageBlock.find.byKey("First Page");
        assertEquals(1, page.getSubBlocks().size());
        assertTrue(page.getSubBlocks().get(0) instanceof CollectionBlock);

    }

    /**
     * Test if it's possible to change Blocks Content
     */
    @Test
    public void changeBlocksContent() {
        ContentBlock content = ContentBlock.find.byKey("First Content");
        assertEquals("I'm the very first content! formerly known as Hello World ;)", content.getContent().get("de"));

        t_begin();
        content.getContent().set("de", "Hello World!");
        content.save();
        t_commitAndClear();

        content = ContentBlock.find.byKey("First Content");

        assertEquals("Hello World!", content.getContent().get("de"));
    }


    /**
     * Test if it's possible to delete Blocks MString
     * Test should throw an Exception since MString is marked as required
     */
    @Test
    public void deleteBlocksMString() {

        ContentBlock content = ContentBlock.find.byKey("First Content");

        try {
            t_begin();
            content.getContent().delete();
            t_commitAndClear();
            Assert.fail();
        } catch(RollbackException e) {
            assertTrue(e.getMessage().contains("Referential integrity constraint violation"));
        }

        content = ContentBlock.find.byKey("First Content");
        assertNotNull(content.getContent());
    }


    @Test
    public void timeRestrictionTests() {
        // Running these in one test serially
        timeRestrictionFuture();
        timeRestrictionPast();
        timeRestrictionPresent();
        timeRestrictionNone();
    }

    private void timeRestrictionFuture() {

        ContentBlock content = ContentBlock.find.byKey("First Content");

        t_begin();
        Date from = new DateTime().plusHours(1).toDate();
        Date to = new DateTime().plusHours(3).toDate();

        content.setDisplayFrom(from);
        content.setDisplayTo(to);
        content.save();
        t_commitAndClear();

        content = ContentBlock.find.byKey("First Content");

        assertFalse("timeRestrictionFuture - content.checkTimeRestriction()", content.checkTimeRestriction());
        assertFalse("timeRestrictionFuture - content.canRead()", content.canRead());

        List<ContentBlock> list = new ArrayList<>();
        list.add(content);
        assertEquals("timeRestrictionFuture - Template.filterPermitted()", 0, Template.filterPermitted(list).size());

    }

    private void timeRestrictionPresent() {

        ContentBlock content = ContentBlock.find.byKey("First Content");

        t_begin();
        Date from = new DateTime().minusHours(1).toDate();
        Date to = new DateTime().plusHours(3).toDate();

        content.setDisplayFrom(from);
        content.setDisplayTo(to);
        content.save();
        t_commitAndClear();

        content = ContentBlock.find.byKey("First Content");

        assertTrue("timeRestrictionPresent - content.checkTimeRestriction()", content.checkTimeRestriction());
        assertTrue("timeRestrictionPresent - content.canRead()", content.canRead());

        List<ContentBlock> list = new ArrayList<>();
        list.add(content);
        assertEquals("timeRestrictionPresent - Template.filterPermitted()", 1, Template.filterPermitted(list).size());

    }

    private void timeRestrictionPast() {

        ContentBlock content = ContentBlock.find.byKey("First Content");

        t_begin();
        Date from = new DateTime().minusHours(1).toDate();
        Date to = new DateTime().minusHours(3).toDate();

        content.setDisplayFrom(from);
        content.setDisplayTo(to);
        content.save();
        t_commitAndClear();

        content = ContentBlock.find.byKey("First Content");

        assertFalse("timeRestrictionPast - content.checkTimeRestriction()", content.checkTimeRestriction());
        assertFalse("timeRestrictionPast - content.canRead()", content.canRead());

        List<ContentBlock> list = new ArrayList<>();
        list.add(content);
        assertEquals("timeRestrictionFuture - Template.filterPermitted()", 0, Template.filterPermitted(list).size());
    }


    private void timeRestrictionNone() {

        ContentBlock content = ContentBlock.find.byKey("First Content");

        t_begin();

        content.setDisplayFrom(null);
        content.setDisplayTo(null);
        content.save();
        t_commitAndClear();

        content = ContentBlock.find.byKey("First Content");

        assertTrue("timeRestrictionNone - content.checkTimeRestriction()", content.checkTimeRestriction());
        assertTrue("timeRestrictionNone - content.canRead()", content.canRead());

        List<ContentBlock> list = new ArrayList<>();
        list.add(content);
        assertEquals("timeRestrictionNone - Template.filterPermitted()", 1, Template.filterPermitted(list).size());

    }


    @Test
    public void readAccess() {

        final PageBlock unrestrictedPage = PageBlock.find.byKey("First Page"); // unrestricted
        final PageBlock readRestrictedPage = PageBlock.find.byKey("Second Page"); // read-restricted
        final PageBlock inheritingPage = PageBlock.find.byKey("Third Page"); // inherits rights from readRestrictedPage

	    final PageBlock trashedPage = PageBlock.find.byKey("Trashed Page"); // inherits rights from Trash
	    final PageBlock trashedSubPage = PageBlock.find.byKey("Trashed Subpage");

	    final PageBlock draftPage = PageBlock.find.byKey("Draft Page"); // inherits rights from Drafts
	    final PageBlock draftSubPage = PageBlock.find.byKey("Draft Subpage");

	    final PageBlock invisiblePage = PageBlock.find.byKey("Invisible Page");

        final Party superUser = PlayAuth.getPartyManager().findOneByPrincipal(new EmailAddress("super@insign.ch"));
        final Party readerUser = PlayAuth.getPartyManager().findOneByPrincipal(new EmailAddress("reader@insign.ch"));
        final Party writerUser = PlayAuth.getPartyManager().findOneByPrincipal(new EmailAddress("writer@insign.ch"));
	    final Party unprivilegedUser = PlayAuth.getPartyManager().findOneByPrincipal(new EmailAddress("unprivileged@insign.ch"));

        // Super user
        PlayAuth.executeWithTransactionAs(superUser, () -> {
            assertTrue("Super user can read \"First Page\"", unrestrictedPage.canRead());
            assertTrue("Super user can get \"First Page\" through filter", Template.filterPermitted(Arrays.asList(unrestrictedPage)).size() == 1);
            assertTrue("Super user can read \"Second Page\"", readRestrictedPage.canRead());
            assertTrue("Super user can get \"Second Page\" through filter", Template.filterPermitted(Arrays.asList(readRestrictedPage)).size() == 1);
            assertTrue("Super user can read \"Third Page\"", inheritingPage.canRead());
            assertTrue("Super user can get \"Third Page\" through filter", Template.filterPermitted(Arrays.asList(inheritingPage)).size() == 1);

            assertTrue("Super user can read \"Trashed Page\"", trashedPage.canRead());
            assertTrue("Super user can read \"Trashed SubPage\"", trashedSubPage.canRead());

            assertTrue("Super user can read \"Draft Page\"", draftPage.canRead());
            assertTrue("Super user can read \"Draft SubPage\"", draftSubPage.canRead());

            Result result = play.test.Helpers.route(withAuthenticatedParty(
                    ch.insign.cms.controllers.routes.FrontendController.page(invisiblePage.getNavItem("en").getId()),
                    superUser
            ));

            assertEquals("Super user can read \"Invisible Page\"", OK, status(result));
        });

        // Reader user
        PlayAuth.executeWithTransactionAs(readerUser, () -> {
            assertTrue("Reader user can read \"First Page\"", unrestrictedPage.canRead());
            assertTrue("Reader user can get \"First Page\" through filter", Template.filterPermitted(Arrays.asList(unrestrictedPage)).size() == 1);
            assertTrue("Reader user can read \"Second Page\"", readRestrictedPage.canRead());
            assertTrue("Reader user can get \"Second Page\" through filter", Template.filterPermitted(Arrays.asList(readRestrictedPage)).size() == 1);
            assertTrue("Reader user can read \"Third Page\"", inheritingPage.canRead());
            assertTrue("Reader user can get \"Third Page\" through filter", Template.filterPermitted(Arrays.asList(inheritingPage)).size() == 1);

            assertTrue("Reader user can read \"Trashed Page\"", trashedPage.canRead());
            assertTrue("Reader user can read \"Trashed SubPage\"", trashedSubPage.canRead());

            assertTrue("Reader user can read \"Draft Page\"", draftPage.canRead());
            assertTrue("Reader user can read \"Draft SubPage\"", draftSubPage.canRead());

            // FIXME: assertEquals("Reader user can NOT read \"Invisible Page\"", NOT_FOUND, status(result));
        });

        // Writer user
        PlayAuth.executeWithTransactionAs(writerUser, () -> {
            assertTrue("Writer user can read \"First Page\"", unrestrictedPage.canRead());
            assertTrue("Writer user can get \"First Page\" through filter", Template.filterPermitted(Arrays.asList(unrestrictedPage)).size() == 1);
            assertTrue("Writer user can read \"Second Page\"", readRestrictedPage.canRead());
            assertTrue("Writer user can get \"Second Page\" through filter", Template.filterPermitted(Arrays.asList(readRestrictedPage)).size() == 1);
            assertTrue("Writer user can read \"Third Page\"", inheritingPage.canRead());
            assertTrue("Writer user can get \"Third Page\" through filter", Template.filterPermitted(Arrays.asList(inheritingPage)).size() == 1);

            assertTrue("Writer user can read \"Trashed Page\"", trashedPage.canRead());
            assertTrue("Writer user can read \"Trashed SubPage\"", trashedSubPage.canRead());

            assertTrue("Writer user can read \"Draft Page\"", draftPage.canRead());
            assertTrue("Writer user can read \"Draft SubPage\"", draftSubPage.canRead());

            Result result = route(withAuthenticatedParty(
                    ch.insign.cms.controllers.routes.FrontendController.page(invisiblePage.getNavItem("en").getId()),
                    writerUser
            ));

            assertEquals("Writer user can read \"Invisible Page\"", OK, status(result));
        });

	    // Unprivileged user
	    PlayAuth.executeWithTransactionAs(unprivilegedUser, () -> {
            assertTrue("Unprivileged user can read \"First Page\"", unrestrictedPage.canRead());
            assertTrue("Unprivileged user can get \"First Page\" through filter", Template.filterPermitted(Arrays.asList(unrestrictedPage)).size() == 1);
            assertTrue("Unprivileged user can NOT read \"Second Page\"", !readRestrictedPage.canRead());
            assertTrue("Unprivileged user can NOT get \"Second Page\" through filter", Template.filterPermitted(Arrays.asList(readRestrictedPage)).isEmpty());
            assertTrue("Unprivileged user can NOT read \"Third Page\"", !inheritingPage.canRead());
            assertTrue("Unprivileged user can NOT get \"Third Page\" through filter", Template.filterPermitted(Arrays.asList(inheritingPage)).isEmpty());

            assertFalse("Unprivileged user can NOT read \"Trashed Page\"", trashedPage.canRead());
            assertFalse("Unprivileged user can NOT read \"Trashed SubPage\"", trashedSubPage.canRead());

            assertFalse("Unprivileged user can NOT read \"Draft Page\"", draftPage.canRead());
            assertFalse("Unprivileged user can NOT read \"Draft SubPage\"", draftSubPage.canRead());

            // FIXME: assertEquals("Unprivileged user can NOT read \"Invisible Page\"", NOT_FOUND, status(result));
        });

        // Anonymous user
        PlayAuth.executeWithTransactionAs(null, () -> {
            assertTrue("Anonymous user can read \"First Page\"", unrestrictedPage.canRead());
            assertTrue("Anonymous user can get \"First Page\" through filter", Template.filterPermitted(Arrays.asList(unrestrictedPage)).size() == 1);
            assertTrue("Anonymous user can NOT read \"Second Page\"", !readRestrictedPage.canRead());
            assertTrue("Anonymous user can NOT get \"Second Page\" through filter", Template.filterPermitted(Arrays.asList(readRestrictedPage)).isEmpty());
            assertTrue("Anonymous user can NOT read \"Third Page\"", !inheritingPage.canRead());
            assertTrue("Anonymous user can NOT get \"Third Page\" through filter", Template.filterPermitted(Arrays.asList(inheritingPage)).isEmpty());

            assertFalse("Anonymous user cannot read \"Trashed Page\"", trashedPage.canRead());
            assertFalse("Anonymous user cannot read \"Trashed SubPage\"", trashedSubPage.canRead());

            assertFalse("Anonymous user cannot read \"Draft Page\"", draftPage.canRead());
            assertFalse("Anonymous user cannot read \"Draft SubPage\"", draftSubPage.canRead());

            // FIXME: assertEquals("Anonymous user can NOT read \"Invisible Page\"", NOT_FOUND, status(result));
        });
    }

    @Test
    public void writeAccess() {

        final PageBlock p1 = PageBlock.find.byKey("First Page"); // unrestricted
        final PageBlock p2 = PageBlock.find.byKey("Second Page"); // read-restricted
        final PageBlock p3 = PageBlock.find.byKey("Third Page"); // inherits rights from p2 (only when unrestricted)

        final Party u1 = PlayAuth.getPartyManager().findOneByPrincipal(new EmailAddress("super@insign.ch"));
        final Party u2 = PlayAuth.getPartyManager().findOneByPrincipal(new EmailAddress("reader@insign.ch"));
        final Party u3 = PlayAuth.getPartyManager().findOneByPrincipal(new EmailAddress("writer@insign.ch"));

        // Anonymous user
        assertTrue("Anonymous user can NOT modify \"First Page\"", !p1.canModify());
        assertTrue("Anonymous user can NOT modify \"Second Page\"", !p2.canModify());
        assertTrue("Anonymous user can NOT modify \"Third Page\"", !p3.canModify());

        // Super user
        PlayAuth.executeWithTransactionAs(u1, () -> {
            assertTrue("Super user can modify \"First Page\"", p1.canModify());
            assertTrue("Super user can modify \"Second Page\"", p2.canModify());
            assertTrue("Super user can modify \"Third Page\"", p3.canModify());
        });

        // Reader user
        PlayAuth.executeWithTransactionAs(u2, () -> {
            assertTrue("Reader user can NOT modify \"First Page\"", !p1.canModify());
            assertTrue("Reader user can modify \"Second Page\"", p2.canModify());
            assertTrue("Reader user can NOT modify \"Third Page\"", !p3.canModify());
        });

        // Writer user
        PlayAuth.executeWithTransactionAs(u3, () -> {
            assertTrue("Writer user can modify \"First Page\"", p1.canModify());
            assertTrue("Writer user can modify \"Second Page\"", p2.canModify());
            assertTrue("Writer user can modify \"Third Page\"", p3.canModify());
        });

        // Repeat to test permission caching

        // Anonymous user
        PlayAuth.executeWithTransactionAs(null, () -> {
            assertTrue("Anonymous user can NOT modify \"First Page\"", !p1.canModify());
            assertTrue("Anonymous user can NOT modify \"Second Page\"", !p2.canModify());
            assertTrue("Anonymous user can NOT modify \"Third Page\"", !p3.canModify());
        });

        // Writer user
        PlayAuth.executeWithTransactionAs(u3, () -> {
            assertTrue("Writer user can modify \"First Page\"", p1.canModify());
            assertTrue("Writer user can modify \"Second Page\"", p2.canModify());
            assertTrue("Writer user can modify \"Third Page\"", p3.canModify());
        });
    }

    @Test
    public void cachedAccess() {
        if (Configuration.getOrElse("cms.blockcache.enabled", false)) {
            JPA.withTransaction(() -> {

                final PageBlock page = new PageBlock();
                page.setName("Test Page");
                page.setKey("Test Page");
                page.save();

                final Party user = PlayAuth.getPartyManager().create(
                        "User", "secretword", new EmailAddress("User@insign.ch"), ISOGender.MALE, PartyType.PERSON);

                Language.forceCurrentLanguage("en");

                // Unprivileged user & unrestricted page

                PlayAuth.executeAs(user, () -> {

                    assertNull("The page is NOT cached before the first access", page.cache().get());
                    page.cached(); // first access
                    assertNotNull("The page is cached after the first access", page.cache().get());

                    assertTrue("Unprivileged user can read \"Test Page\"", page.canRead());
                    assertTrue("Unprivileged user can NOT modify \"Test Page\"", !page.canModify());
                });

                // Unprivileged user & restricted page

                getAccessControlManager().denyPermission(SecurityIdentity.ALL, BlockPermission.READ, page);
                PlayAuth.executeAs(user, () -> {

                    assertNull("The page is NOT cached before the first access", page.cache().get());
                    page.cached(); // first access
                    assertNotNull("The page is cached after the first access", page.cache().get());

                    assertTrue("Unprivileged user can NOT read \"Test Page\"", !page.canRead());
                    assertTrue("Unprivileged user can NOT modify \"Test Page\"", !page.canModify());
                });

                // User with "read" permission & restricted page

                getAccessControlManager().allowPermission(user, BlockPermission.READ, page);
                PlayAuth.executeAs(user, () -> {

                    assertNull("The page is NOT cached before the first access", page.cache().get());
                    page.cached(); // first access
                    assertNotNull("The page is cached after the first access", page.cache().get());

                    assertTrue("User with \"read\" permission can read \"Test Page\"", page.canRead());
                    assertTrue("User with \"modify\" permission can NOT modify \"Test Page\"", !page.canModify());
                });

                // User without "read" permission & restricted page

                getAccessControlManager().denyPermission(user, BlockPermission.READ, page);
                PlayAuth.executeAs(user, () -> {
                    assertTrue("User without \"read\" permission can NOT read \"Test Page\"", !page.canRead());
                    assertTrue("User without \"read\" permission can NOT modify \"Test Page\"", !page.canModify());
                });
            });
        }
    }

	/**
	 * TODO: encapsulate this utility into play-auth
	 */
    public Http.RequestBuilder withAuthenticatedParty(play.mvc.Call call, Party party) {
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder();
        requestBuilder.uri(call.url());

        Http.Session playSession = new Http.Session(new HashMap<>());
        Session shiroSession = new PlayShiroSession(playSession, null);
        shiroSession.setAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY, party.getPrincipals());
        playSession.forEach(requestBuilder::session);
        return requestBuilder;
    }
}
